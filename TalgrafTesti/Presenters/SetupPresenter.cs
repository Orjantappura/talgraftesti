﻿using System;
using System.Collections.Generic;
using System.IO;
using TalgrafTesti.Views;
using TalgrafTesti.Models;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using System.ComponentModel;

namespace TalgrafTesti.Presenters
{
    public class SetupPresenter
    {
        //Variables for creating dummy files
        private int numberOfFiles; //number of files to be created
        private string currentDirectory;
        private string folderName;
        private string folderPath;
        private string subFolderName;
        private string subFolderPath;

        ISetupView setupView;
        SetupModel setupModel;
        BackgroundWorker installationWorker;
        public event EventHandler InstallationComplete;

        //Variable for counting copied files for installation progress
        private int filesCopied;

        public SetupPresenter(ISetupView view, SetupModel model)
        {
            setupView = view;
            setupModel = model;
            SetViewPropertiesFromModel();
            BindViewEvents();

            //Setup and create dummy files
            numberOfFiles = 85;
            currentDirectory = Directory.GetCurrentDirectory();
            folderName = "Tiedostot";
            subFolderName = "Kansio";
            folderPath = Path.Combine(currentDirectory, folderName);
            subFolderPath = Path.Combine(folderPath, subFolderName);
            CreateDummyFiles();
        }

        public void CopyInstallationFiles()
        {
            //Use a background worker to copy files asynchronously
            installationWorker = new BackgroundWorker();
            installationWorker.DoWork += InstallationWorker_DoWork;
            installationWorker.ProgressChanged += InstallationWorker_ProgressChanged;
            installationWorker.RunWorkerCompleted += InstallationWorker_RunWorkerCompleted;
            installationWorker.WorkerReportsProgress = true;
            installationWorker.WorkerSupportsCancellation = true;
            installationWorker.RunWorkerAsync();
        }

        public void DeleteInstallationFiles()
        {
            //Handle installation being cancelled

            //Check is background worker is working
            if (installationWorker.IsBusy)
            {
                //Set cancel pending on the worker
                installationWorker.CancelAsync();
            }

            string targetPath = setupModel.InstallationPath;
            if (!Directory.Exists(targetPath))
            {
                //Nothing to delete, return
                return;
            }

            //Start deleting files and folder
            foreach (string file in setupModel.FileList)
            {
                if (File.Exists(file.Replace(folderPath, targetPath)))
                {
                    try
                    {
                        File.Delete(file.Replace(folderPath, targetPath));
                    }
                    catch (IOException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }

            //Delete subfolders
            DirectoryInfo dirInfo = new DirectoryInfo(targetPath);
            foreach (DirectoryInfo dir in dirInfo.GetDirectories())
            {
                try
                {
                    dir.Delete();
                }
                catch (IOException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            //Finally, delete the installation folder
            try
            {
                Directory.Delete(targetPath);
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void InstallationWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //Handle copying the files

            BackgroundWorker bgWorker = sender as BackgroundWorker;

            //Get installation path from the model
            string targetPath = setupModel.InstallationPath;

            //Create installation directories
            foreach (string dirPath in Directory.GetDirectories(folderPath, "*", SearchOption.AllDirectories))
            {
                if (!Directory.Exists(dirPath.Replace(folderPath, targetPath)))
                {
                    Directory.CreateDirectory(dirPath.Replace(folderPath, targetPath));
                }
            }

            filesCopied = 0; //Reset copied files counter

            //Copy the files
            foreach (string file in setupModel.FileList)
            {
                //Check is work has been cancelled
                if (bgWorker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                try
                {
                    File.Copy(file, file.Replace(folderPath, targetPath), true);
                }
                catch (System.IO.IOException ex)
                {
                    Console.WriteLine(ex.Message);
                }

                double progress = (double)filesCopied / (double)numberOfFiles * 100;
                installationWorker.ReportProgress((int)progress);

                //Use thread sleep to simulate slower progress
                Thread.Sleep(100);

                filesCopied++;
            }
        }

        private void InstallationWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            setupView.InfoLabel.Content = "Kopioidaan tiedostoa " + 
                Path.GetFileName(setupModel.FileList[filesCopied]) + "... [" + filesCopied.ToString() + "//" + numberOfFiles.ToString() + "]";
            setupView.InstallationProgress.Value = e.ProgressPercentage;
        }

        private void InstallationWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //Reset progress if installation was cancelled
                setupView.InstallationProgress.Value = 0;
            }
            else
            {
                InstallationComplete?.Invoke(sender, e);
            }
        }

        public string ShowChoosePathDialog()
        {
            //Dialog for choosing installation folder when clicking the browse-button
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    return fbd.SelectedPath;
                }
                else
                {
                    //If dialog result is not OK, return the current old installation path
                    return setupModel.InstallationPath;
                }
            }
        }

        private void SetModelPropertiesFromView()
        {
            foreach (PropertyInfo viewProperty in setupView.GetType().GetProperties())
            {
                if (viewProperty.CanRead)
                {
                    PropertyInfo modelProperty = setupModel.GetType().GetProperty(viewProperty.Name);

                    if (modelProperty != null && modelProperty.PropertyType.Equals(viewProperty.PropertyType))
                    {
                        object valueToAssign = Convert.ChangeType(viewProperty.GetValue(setupView, null), modelProperty.PropertyType);

                        if (valueToAssign != null)
                        {
                            modelProperty.SetValue(setupModel, valueToAssign, null);
                        }
                    }
                }
            }
        }

        private void SetViewPropertiesFromModel()
        {
            foreach (PropertyInfo viewProperty in setupView.GetType().GetProperties())
            {
                if (viewProperty.CanWrite)
                {
                    PropertyInfo modelProperty = setupModel.GetType().GetProperty(viewProperty.Name);

                    if (modelProperty != null && modelProperty.PropertyType.Equals(viewProperty.PropertyType))
                    {
                        object modelValue = modelProperty.GetValue(setupModel, null);

                        if (modelValue != null)
                        {
                            object valueToAssign = Convert.ChangeType(modelValue, viewProperty.PropertyType);

                            if (valueToAssign != null)
                            {
                                viewProperty.SetValue(setupView, valueToAssign, null);
                            }
                        }
                    }
                }
            }
        }

        private void CreateDummyFiles()
        {
            //Create a folder for files in the Setup.exe folder
            Directory.CreateDirectory(folderPath);
            Directory.CreateDirectory(subFolderPath);

            //Create files, create half of the files in the subfolder
            for (int i = 0; i < numberOfFiles; i++)
            {
                if (i < numberOfFiles / 2)
                {
                    string filePathString = Path.Combine(folderPath, "File") + i.ToString();
                    if (!File.Exists(filePathString))
                    {
                        using (FileStream fs = File.Create(filePathString))
                        {
                            for (byte b = 0; b < 10; b++)
                            {
                                //Write some bytes into each file
                                fs.WriteByte(b);
                            }
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    string filePathString = Path.Combine(subFolderPath, "File") + i.ToString();
                    if (!File.Exists(filePathString))
                    {
                        using (FileStream fs = File.Create(filePathString))
                        {
                            for (byte b = 0; b < 10; b++)
                            {
                                //Write some bytes into each file
                                fs.WriteByte(b);
                            }
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            PopulateFileList();
        }

        private void PopulateFileList()
        {
            //Get files from .\Tiedostot\ and its subfolders and update the model
            setupModel.FileList = new List<string>(Directory.GetFiles(folderPath, "*", SearchOption.AllDirectories));
        }

        private void BindViewEvents()
        {
            setupView.InstallationPathChanged += SetupView_InstallationPathChanged;
        }

        private void SetupView_InstallationPathChanged(object sender, EventArgs e)
        {
            //Update model
            SetModelPropertiesFromView();
        }
    }
}
