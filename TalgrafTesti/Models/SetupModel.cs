﻿using System.Collections.Generic;

namespace TalgrafTesti.Models
{
    public class SetupModel : ISetupModel
    {
        
        private List<string> fileList;

        private string installationPath;

        public SetupModel()
        {
            installationPath = @"C:\Ohjelma"; //Default installation path
        }

        public List<string> FileList
        {
            get { return fileList; }
            set { fileList = value; }
        }

        public string InstallationPath
        {
            get { return installationPath; }
            set { installationPath = value; }
        }
    }
}
