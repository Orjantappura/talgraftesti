﻿using System.Collections.Generic;

namespace TalgrafTesti.Models
{
    public interface ISetupModel
    {
        List<string> FileList { get; set; }
        string InstallationPath { get; set; }
    }
}
