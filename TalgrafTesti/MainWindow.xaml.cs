﻿/*
 * Simple installer using the MVP design pattern 
 * by Olli Koskenranta
 */

using System;
using System.Windows;
using System.Windows.Controls;
using TalgrafTesti.Presenters;
using TalgrafTesti.Views;

namespace TalgrafTesti
{

    public partial class MainWindow : Window, ISetupView
    {
        private int installationStage;
        SetupPresenter setupPresenter;
        public event EventHandler InstallationPathChanged;
        public event EventHandler InstallationStageChanged;

        public MainWindow()
        {
            InitializeComponent();


            setupPresenter = new SetupPresenter(this, new Models.SetupModel());
            setupPresenter.InstallationComplete += SetupPresenter_InstallationComplete;
            InstallationStageChanged += MainWindow_InstallationStageChanged;

            //Setup welcome view
            if (InstallationStage == 0)
            {
                lblInfo.Content = "Tervetuloa Ohjelman asennusohjelmaan!";
                pbInstallationProgress.Visibility = Visibility.Hidden;
                tbInstallationDirectory.Visibility = Visibility.Hidden;
                btnBrowse.Visibility = Visibility.Hidden;
            }
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            //Proceed forward in setup if installation stage below 3
            if (InstallationStage < 3)
            {
                InstallationStage++;
            }

            //If cancel button is clicked, return to choose path view and delete already installed files
            else if (InstallationStage == 3)
            {
                InstallationStage = 1;
                setupPresenter.DeleteInstallationFiles();
            }

            //Else set installation stage to "finished" for closing program
            else
            {
                InstallationStage = 5;
            }

            InstallationStageChanged?.Invoke(sender, e);
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            tbInstallationDirectory.Text = setupPresenter.ShowChoosePathDialog();
        }

        public string InstallationPath
        {
            get { return tbInstallationDirectory.Text.ToString(); }
            set { tbInstallationDirectory.Text = value; }
        }

        public int InstallationStage
        {
            get { return installationStage; }
            set { installationStage = value; }
        }

        public ProgressBar InstallationProgress
        {
            get { return pbInstallationProgress; }
        }

        public Label InfoLabel
        {
            get { return lblInfo; }
        }

        private void tbInstallationDirectory_TextChanged(object sender, TextChangedEventArgs e)
        {
            InstallationPathChanged?.Invoke(sender, e);
        }

        private void SetupPresenter_InstallationComplete(object sender, EventArgs e)
        {
            pbInstallationProgress.Value = 100;
            installationStage = 4;
            InstallationStageChanged?.Invoke(sender, e);
        }

        private void MainWindow_InstallationStageChanged(object sender, EventArgs e)
        {
            switch (InstallationStage)
            {
                case 0:
                    //Welcome view
                    lblInfo.Content = "Tervetuloa Ohjelman asennusohjelmaan!";
                    pbInstallationProgress.Visibility = Visibility.Hidden;
                    tbInstallationDirectory.Visibility = Visibility.Hidden;
                    btnBrowse.Visibility = Visibility.Hidden;
                    btnNext.Content = "Seuraava";
                    break;
                case 1:
                    //Choose path view
                    lblInfo.Content = "Valitse ohjelman asennuskansio:";
                    pbInstallationProgress.Visibility = Visibility.Hidden;
                    tbInstallationDirectory.Visibility = Visibility.Visible;
                    btnBrowse.Visibility = Visibility.Visible;
                    btnNext.Content = "Seuraava";
                    break;
                case 2:
                    //Ready for installation view
                    lblInfo.Content = "Valmis asennukseen kohteeseen " + InstallationPath;
                    pbInstallationProgress.Visibility = Visibility.Visible;
                    tbInstallationDirectory.Visibility = Visibility.Hidden;
                    btnBrowse.Visibility = Visibility.Hidden;
                    btnNext.Content = "Asenna";
                    break;
                case 3:
                    //Installing view
                    lblInfo.Content = "Asennetaan...";
                    setupPresenter.CopyInstallationFiles();
                    btnNext.Content = "Peruuta";
                    break;
                case 4:
                    //Installation complete, ready to close program
                    lblInfo.Content = "Asennus valmis!";
                    btnNext.Content = "Lopeta";
                    break;
                case 5:
                    //Close button was pressed
                    this.Close();
                    break;
                default:
                    break;
            }
        }
    }
}
