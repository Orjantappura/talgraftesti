﻿using System;
using System.Windows.Controls;

namespace TalgrafTesti.Views
{
    public interface ISetupView
    {
        event EventHandler InstallationPathChanged;
        event EventHandler InstallationStageChanged;
        string InstallationPath { get; set; }
        int InstallationStage { get; set; }
        ProgressBar InstallationProgress { get; }
        Label InfoLabel { get; }
    }
}
